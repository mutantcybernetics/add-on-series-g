# Add-on-Series-G

# GPS + WiFi + IR Reciver + 2xNeoPixel + Humidity + Pressure + Temperature  

[See parts list from more info](https://gitlab.com/mutantC/add-on-series-g/-/blob/main/parts_list)

It has this module support (Click the blue color name to show the module)
- [Ubox-GPS](https://www.ebay.com/sch/i.html?_nkw=gyneo6mv2) (only module, no PCB)
- [ESP-8266](https://www.ebay.com/sch/i.html?_nkw=ESP8266+Serial+WIFI+Wireless+Transceiver+Module)
- [BME-280](https://www.aliexpress.com/item/4001110744837.html) (1027/1030/1034)
- [IR Reciver](https://www.aliexpress.com/item/4000184301216.html)
- [NeoPixel](https://www.ebay.com/sch/i.html?_nkw=mpu6050) (WS2812B 3535(SK6812 Mini-HS)


<img src="top.png" width="500">
<img src="bottom.png" width="500">
<img src="pic_main.png" width="500">
<img src="position.png" width="500">
<img src="show_off.png" width="500">
